import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';

describe('App', () => {
  // Smoke test:
  // This test mounts a component and makes sure that it does not throw during rendering.
  // Tests like this provide a lot of value with very little effort so they are great as a starting point.
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
  });
});
