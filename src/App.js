import React, { lazy, Suspense } from 'react';
import Container from '@material-ui/core/Container';
import { CssBaseline } from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import AppHeader from './components/AppHeader';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import TrackerAbout from './pages/TrackerAbout';
import PageFooter from './components/PageFooter';

const TrackerStats = lazy(() => import('./pages/TrackerStats'));

const theme = createMuiTheme({
  palette: {
    primary: {
      main: 'rgba(0,0,127, 0.8)',
      mainLight: 'rgba(0,0,127, 0.2)',
      darkRed: '#D61F1F',
      lightRed: 'rgba(255, 0, 0, 0.5)',
      white: 'white',
      grey: '#cccccc',
      green: 'green',
    },
  },
});

const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline>
        <Router>
          <AppHeader id="timeline" title="COVID-19 Tracker" />
          <Container maxWidth="lg">
            <div
              style={{
                display: 'flex',
                minHeight: `calc(100vh - ${theme.mixins.toolbar.minHeight}px)`,
                flexDirection: 'column',
              }}
            >
              <Switch>
                <Route path="/about">
                  <TrackerAbout />
                </Route>
                <Route path="/">
                  <Suspense fallback={<div className="loader"></div>}>
                    <TrackerStats />
                  </Suspense>
                </Route>
              </Switch>
              <PageFooter />
            </div>
          </Container>
        </Router>
      </CssBaseline>
    </MuiThemeProvider>
  );
};

export default App;
