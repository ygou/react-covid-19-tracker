import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import PageTitle from '../components/PageTitle';

const useStyles = makeStyles({
  container: {
    width: '100%',
    maxWidth: 480,
    margin: '0 auto',
  },
  mailButton: {
    padding: '0 12px',
  },
});

function ListItemLink(props) {
  return (
    <ListItem button component="a" {...props} target="_blank" rel="noopener" />
  );
}

function TrackerAbout() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <PageTitle title="About COVID-19 Tracker" />
      <Typography variant="body1" gutterBottom paragraph>
        COVID-19 Tracker is developed mainly to help find the daily COVID-19
        cases by a given region or country.
      </Typography>
      <Typography variant="body1" gutterBottom paragraph>
        This app also shows the spread of COVID-19 on a world map, so as to give
        a bird's eye view on the pandemic globally.
      </Typography>
      <Typography variant="h5" variantMapping={{ h5: 'h2' }}>
        COVID-19 Tracker Public API
      </Typography>
      <List component="nav" aria-label="secondary mailbox folders">
        <ListItemLink href="https://covid19-api.codegoodworks.co.uk/api/regions">
          <ListItemText primary="1. Get all available regions / countries" />
        </ListItemLink>
        <ListItemLink href="https://covid19-api.codegoodworks.co.uk/api/daily/United%20Kingdom">
          <ListItemText primary="2. Get all daily cases for given region / country" />
        </ListItemLink>
        <ListItemLink href="https://covid19-api.codegoodworks.co.uk/api/passed/day">
          <ListItemText primary="3. Get increased / descreased cases on the passed day for every region and country" />
        </ListItemLink>
        <ListItemLink href="https://covid19-api.codegoodworks.co.uk/api/passed/week">
          <ListItemText primary="4. Get increased / descreased cases in the passed week for every region and country" />
        </ListItemLink>
        <ListItemLink href="https://covid19-api.codegoodworks.co.uk/api/passed/month">
          <ListItemText primary="5. Get increased / descreased cases in the passed month for every region and country" />
        </ListItemLink>
        <ListItemLink href="https://covid19-api.codegoodworks.co.uk/api/passed/alltime">
          <ListItemText primary="6. Get the current cases for every region and country" />
        </ListItemLink>
      </List>
      <Typography
        variant="h5"
        variantMapping={{ h5: 'h2' }}
        gutterBottom
        paragraph
      >
        Acknowledgement
      </Typography>
      <Typography variant="body1" gutterBottom paragraph>
        Special thanks to <em>&#64;mathdro</em> for providing{' '}
        <Link
          href="https://covid19.mathdro.id/api"
          target="_blank"
          rel="noopener"
        >
          the COVID-19 global data (from JHU CSSE for now) as-a-service
        </Link>
        , from which this app collects the COVID-19 data. As the mathdro API
        does not directly provide the daily cases for a given region or country,
        COVID-19 Tracker is created to make this available.
      </Typography>
      <Typography variant="body1" gutterBottom paragraph>
        And thanks to <em>&#64;ashkyd</em> for providing{' '}
        <Link
          href="https://geojson-maps.ash.ms/"
          target="_blank"
          rel="noopener"
        >
          the vector world map
        </Link>
        .
      </Typography>
      <Typography
        variant="h5"
        variantMapping={{ h5: 'h2' }}
        gutterBottom
        paragraph
      >
        Contact Me
      </Typography>
      <Typography variant="body1" gutterBottom paragraph>
        Should you have any suggestions or questions, please don't hesitate to
        contact me{' '}
        <FormControlLabel
          control={
            <a
              target="_top"
              rel="noopener noreferrer"
              href="mailto:yuci@codegoodworks.dev"
            >
              <IconButton
                color="primary"
                className={classes.mailButton}
                aria-label="email"
              >
                <MailOutlineIcon />
              </IconButton>
            </a>
          }
          labelPlacement="end"
        />
      </Typography>
    </div>
  );
}

export default TrackerAbout;
