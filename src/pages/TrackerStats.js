import React, { useState, useEffect } from 'react';
import SectionTitle from '../components/SectionTitle';
import AutocompleteComboBox from '../components/AutocompleteComboBox';
import PageTitle from '../components/PageTitle';
import TimelineChart from '../components/TimelineChart';
import WorldMapChart from '../components/WorldMapChart';
import {
  GLOBAL_REGION,
  Perspectives,
  PerspectivesOptions,
  PerspectiveTitles,
  Timeframes,
  TimeframeOptions,
} from '../config/constants';
import { fetchDailyData, fetchRegions, fetchRegionsDaily } from '../api';
import { totalToDaily } from '../accessories/dataTransformer';
import ControlPanel from '../components/ControlPanel';
import TrackerRadioGroup from '../components/TrackerRadioGroup';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import InfoCard from '../components/InfoCard';

const savedRegion = localStorage.getItem('region');
const savedPerspective = localStorage.getItem('perspective');
const savedTimeframe = localStorage.getItem('timeframe');
const savedTotalOrDaily = localStorage.getItem('totalOrDaily');
const savedLineType = localStorage.getItem('lineType');

const TrackerStats = () => {
  const [dailyData, setDailyData] = useState([]);
  const [selectedRegion, setSelectedRegion] = useState(
    savedRegion ? JSON.parse(savedRegion) : null
  );
  const [perspective, setPerspective] = useState(
    savedPerspective ? savedPerspective : Perspectives.activeOverPopulation
  );
  const [timeframe, setTimeframe] = useState(
    savedTimeframe ? savedTimeframe : Timeframes.passedWeek
  );
  const [regionsDaily, setRegionsDaily] = useState([]);
  const [regions, setRegions] = useState([]);
  const [totalOrDaily, setTotalOrDaily] = useState(
    savedTotalOrDaily ? savedTotalOrDaily : 'daily'
  );
  const [lineType, setLineType] = useState(
    savedLineType ? savedLineType : 'linear'
  );

  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('lg'));

  useEffect(() => {
    fetchRegions().then((data) => {
      data.push(GLOBAL_REGION);
      setRegions(data);
    });
  }, []);

  useEffect(() => {
    fetchDailyData(selectedRegion ? selectedRegion.label : GLOBAL_REGION.label)
      .then((data) => {
        if (totalOrDaily === 'daily') {
          data = totalToDaily(data);
        }
        setDailyData(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [selectedRegion, totalOrDaily]);

  useEffect(() => {
    fetchRegionsDaily(timeframe).then((data) => {
      setRegionsDaily(data);
    });
  }, [timeframe]);

  const handleChange = (event, newValue) => {
    if (newValue) {
      setSelectedRegion(newValue);
      localStorage.setItem('region', JSON.stringify(newValue));
    }
  };
  const handlePerspectiveChange = (event, newValue) => {
    if (newValue) {
      setPerspective(newValue.label);
      localStorage.setItem('perspective', newValue.label);
    }
  };
  const handleTimeframeChange = (event, newValue) => {
    if (newValue) {
      setTimeframe(newValue.label);
      localStorage.setItem('timeframe', newValue.label);
    }
  };

  const handleLineTypeChange = (newValue) => {
    setLineType(newValue);
    localStorage.setItem('lineType', newValue);
  };

  const handleTotalOrDailyChange = (newValue) => {
    setTotalOrDaily(newValue);
    localStorage.setItem('totalOrDaily', newValue);
  };

  return (
    <>
      <PageTitle title="Timeline of Cases" />

      <InfoCard
        dailyData={dailyData}
        region={selectedRegion ? selectedRegion : GLOBAL_REGION}
        totalOrDaily={totalOrDaily}
      />

      <ControlPanel
        components={[
          {
            id: 1,
            xs: 12,
            md: isDesktop ? 6 : 5,
            value: (
              <AutocompleteComboBox
                handleChange={handleChange}
                id="regionSelector"
                label="Choose a region for the timeline"
                options={regions}
                renderOption={(option) => (
                  <>
                    <span style={{ width: '20px' }}>{option.icon}</span>
                    {option.label}
                  </>
                )}
              />
            ),
          },
          {
            id: 2,
            xs: 12,
            md: 3,
            value: (
              <TrackerRadioGroup
                value={totalOrDaily}
                setValue={handleTotalOrDailyChange}
                options={[
                  { value: 'daily', label: 'Daily' },
                  { value: 'total', label: 'Total' },
                ]}
              />
            ),
          },
          {
            id: 3,
            xs: 12,
            md: 'auto',
            value: (
              <TrackerRadioGroup
                value={lineType}
                setValue={handleLineTypeChange}
                options={[
                  { value: 'linear', label: 'Linear' },
                  { value: 'logarithmic', label: 'Logarithmic' },
                ]}
              />
            ),
          },
        ]}
      />

      <TimelineChart
        dailyData={dailyData}
        region={selectedRegion ? selectedRegion.label : GLOBAL_REGION.label}
        type={lineType}
      ></TimelineChart>
      <SectionTitle
        id="spread"
        title={`${PerspectiveTitles[perspective + timeframe]}`}
      />
      <ControlPanel
        components={[
          {
            id: 1,
            xs: 12,
            md: 6,
            value: (
              <AutocompleteComboBox
                handleChange={handlePerspectiveChange}
                id="perspectiveSelector"
                label="Choose a perspective to show on map"
                options={PerspectivesOptions}
                renderOption={(option) => <>{option.label}</>}
              />
            ),
          },
          {
            id: 2,
            xs: 12,
            md: 6,
            value: (
              <AutocompleteComboBox
                handleChange={handleTimeframeChange}
                id="timeframeSelector"
                label="Choose a time frame"
                options={TimeframeOptions}
                renderOption={(option) => <>{option.label}</>}
              />
            ),
          },
        ]}
      />
      <WorldMapChart regionsDaily={regionsDaily} property={perspective} />
    </>
  );
};

export default TrackerStats;
