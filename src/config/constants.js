export const GLOBAL = 'Global';
export const CACHE_TTL = 600; // seconds
export const GLOBAL_REGION = { label: GLOBAL, icon: '🌎' };

export const Timeframes = Object.freeze({
  passedDay: 'Passed Day',
  passedWeek: 'Passed Week',
  passedMonth: 'Passed Month',
  allTime: 'All Time',
});

export const TimeframeOptions = Object.keys(Timeframes).map((p) => {
  return { label: Timeframes[p] };
});

const MATHDRO = 'https://covid19.mathdro.id/api';
const FULLSTACKPRO = 'https://covid19-api.codegoodworks.co.uk/api';

export const URL = {
  WORLD_DAILY: `${MATHDRO}/daily`,
  REGION_DAILY: `${FULLSTACKPRO}/daily`,
  REGIONS: `${FULLSTACKPRO}/regions`,
  [Timeframes.passedDay]: `${FULLSTACKPRO}/passed/day`,
  [Timeframes.passedWeek]: `${FULLSTACKPRO}/passed/week`,
  [Timeframes.passedMonth]: `${FULLSTACKPRO}/passed/month`,
  [Timeframes.allTime]: `${FULLSTACKPRO}/passed/alltime`,
};

export const Perspectives = Object.freeze({
  confirmed: 'Infected',
  confirmedOverPopulation: 'Infected Over Population',
  recovered: 'Recovered',
  recoveredOverConfirmed: 'Recovered Over Infected',
  active: 'Active',
  activeOverPopulation: 'Active Over Population',
  deathToll: 'Death Toll',
  deathsOverConfirmed: 'Deaths Over Infected',
  deathsOverPopulation: 'Deaths Over Population',
});

export const PerspectiveTitles = {
  [`${Perspectives.confirmed}${Timeframes.passedDay}`]: `Infected increased on passed day`,
  [`${Perspectives.confirmed}${Timeframes.passedWeek}`]: `Infected increased in passed week`,
  [`${Perspectives.confirmed}${Timeframes.passedMonth}`]: `Infected increased in passed month`,
  [`${Perspectives.confirmed}${Timeframes.allTime}`]: `Infected in all time`,

  [`${Perspectives.confirmedOverPopulation}${Timeframes.passedDay}`]: `Infected increased Over Population on passed day`,
  [`${Perspectives.confirmedOverPopulation}${Timeframes.passedWeek}`]: `Infected increased Over Population in passed week`,
  [`${Perspectives.confirmedOverPopulation}${Timeframes.passedMonth}`]: `Infected increased Over Population in passed month`,
  [`${Perspectives.confirmedOverPopulation}${Timeframes.allTime}`]: `Infected Over Population in all time`,

  [`${Perspectives.recovered}${Timeframes.passedDay}`]: `Recovered increased on passed day`,
  [`${Perspectives.recovered}${Timeframes.passedWeek}`]: `Recovered increased in passed week`,
  [`${Perspectives.recovered}${Timeframes.passedMonth}`]: `Recovered increased in passed month`,
  [`${Perspectives.recovered}${Timeframes.allTime}`]: `Recovered in all time`,

  [`${Perspectives.recoveredOverConfirmed}${Timeframes.passedDay}`]: `Recovered increased Over Infected increased on passed day`,
  [`${Perspectives.recoveredOverConfirmed}${Timeframes.passedWeek}`]: `Recovered increased Over Infected increased in passed week`,
  [`${Perspectives.recoveredOverConfirmed}${Timeframes.passedMonth}`]: `Recovered increased Over Infected increased in passed month`,
  [`${Perspectives.recoveredOverConfirmed}${Timeframes.allTime}`]: `Recovered Over Infected in all time`,

  [`${Perspectives.active}${Timeframes.passedDay}`]: `Active increased on passed day`,
  [`${Perspectives.active}${Timeframes.passedWeek}`]: `Active increased in passed week`,
  [`${Perspectives.active}${Timeframes.passedMonth}`]: `Active increased in passed month`,
  [`${Perspectives.active}${Timeframes.allTime}`]: `Active now`,

  [`${Perspectives.activeOverPopulation}${Timeframes.passedDay}`]: `Active increased Over Population on passed day`,
  [`${Perspectives.activeOverPopulation}${Timeframes.passedWeek}`]: `Active increased Over Population in passed week`,
  [`${Perspectives.activeOverPopulation}${Timeframes.passedMonth}`]: `Active increased Over Population in passed month`,
  [`${Perspectives.activeOverPopulation}${Timeframes.allTime}`]: `Active now Over Population`,

  [`${Perspectives.deathToll}${Timeframes.passedDay}`]: `Death Toll increased on passed day`,
  [`${Perspectives.deathToll}${Timeframes.passedWeek}`]: `Death Toll increased in passed week`,
  [`${Perspectives.deathToll}${Timeframes.passedMonth}`]: `Death Toll increased in passed month`,
  [`${Perspectives.deathToll}${Timeframes.allTime}`]: `Death Toll in all time`,

  [`${Perspectives.deathsOverConfirmed}${Timeframes.passedDay}`]: `Deaths increased Over Infected increased on passed day`,
  [`${Perspectives.deathsOverConfirmed}${Timeframes.passedWeek}`]: `Deaths increased Over Infected increased in passed week`,
  [`${Perspectives.deathsOverConfirmed}${Timeframes.passedMonth}`]: `Deaths increased Over Infected increased in passed month`,
  [`${Perspectives.deathsOverConfirmed}${Timeframes.allTime}`]: `Deaths Over Infected in all time`,

  [`${Perspectives.deathsOverPopulation}${Timeframes.passedDay}`]: `Deaths increased Over Population on passed day`,
  [`${Perspectives.deathsOverPopulation}${Timeframes.passedWeek}`]: `Deaths increased Over Population in passed week`,
  [`${Perspectives.deathsOverPopulation}${Timeframes.passedMonth}`]: `Deaths increased Over Population in passed month`,
  [`${Perspectives.deathsOverPopulation}${Timeframes.allTime}`]: `Deaths Over Population in all time`,
};

export const PerspectivesOptions = Object.keys(Perspectives).map((p) => {
  return { label: Perspectives[p] };
});
