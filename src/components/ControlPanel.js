import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingTop: '40px',
    paddingBottom: '30px',
  },
}));

function ControlPanel({ components }) {
  const classes = useStyles();
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.down('sm'));

  const gridItemStyle = {
    paddingTop: 0,
    paddingBottom: '10px',
    textAlign: isTablet ? 'left' : 'right',
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={2} justify="space-between" alignItems="center">
        {components.map((comp) => (
          <Grid
            item
            xs={comp.xs}
            md={comp.md}
            style={gridItemStyle}
            key={comp.id}
          >
            {comp.value}
          </Grid>
        ))}
      </Grid>
    </div>
  );
}

export default ControlPanel;
