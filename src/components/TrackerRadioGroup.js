import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  group: {
    border: `1px solid ${theme.palette.primary.grey}`,
    borderRadius: '4px',
    padding: '2px 2px 2px 10px',
  },
}));

function TrackerRadioGroup({ value, setValue, options }) {
  const classes = useStyles();

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <FormControl component="fieldset" className={classes.group}>
      <FormLabel component="legend"></FormLabel>
      <RadioGroup
        aria-label="view"
        name="view"
        value={value}
        onChange={handleChange}
        row
      >
        {options.map((option) => (
          <FormControlLabel
            value={option.value}
            control={<Radio />}
            label={option.label}
            key={option.value}
          />
        ))}
      </RadioGroup>
    </FormControl>
  );
}

export default TrackerRadioGroup;
