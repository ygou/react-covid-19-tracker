import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  title: {
    marginTop: '4rem',
    marginBottom: '2rem',
  },
});

export default function PageTitle({ title }) {
  const classes = useStyles();
  return (
    <div className={classes.title}>
      <Typography
        variant="h4"
        data-testid="page-title"
        gutterBottom={true}
        color="inherit"
        align="center"
        variantMapping={{ h4: 'h1' }}
      >
        {title}
      </Typography>
    </div>
  );
}
