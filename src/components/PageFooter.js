import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: 'auto',
    marginBottom: '1rem',
    textAlign: 'center',
    borderTop: `2px solid ${theme.palette.primary.mainLight}`,
    paddingTop: '3rem',
    paddingBottom: '3rem',
  },
}));

function PageFooter() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Typography variant="body1">
        Copyright © {new Date().getFullYear()}
      </Typography>
      <Typography variant="body1">
        Yuci,{' '}
        <Link
          href="https://codegoodworks.dev"
          color="inherit"
          target="_blank"
          rel="noopener"
        >
          Code Good Works
        </Link>
      </Typography>
    </div>
  );
}

export default PageFooter;
