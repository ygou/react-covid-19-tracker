import React, { useEffect, useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { GLOBAL } from '../config/constants';

const dateTimeFormat = new Intl.DateTimeFormat('en', {
  year: 'numeric',
  month: 'short',
  day: '2-digit',
});

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
    '&:first-letter': {
      textTransform: 'capitalize',
    },
  },
  pos: {
    marginBottom: 12,
  },
});

function InfoCard({ dailyData, region, totalOrDaily }) {
  const [dates, setDates] = useState([]);
  const classes = useStyles();
  const theme = useTheme();
  const isPhone = useMediaQuery(theme.breakpoints.down('xs'));
  const isTablet = useMediaQuery(theme.breakpoints.down('sm'));
  useEffect(() => {
    if (dailyData) {
      if (isPhone) {
        const maxDates = 2;
        if (dailyData.length <= maxDates) {
          setDates(dailyData);
        } else {
          setDates(dailyData.slice(dailyData.length - maxDates));
        }
      } else if (isTablet) {
        const maxDates = 4;
        if (dailyData.length <= maxDates) {
          setDates(dailyData);
        } else {
          setDates(dailyData.slice(dailyData.length - maxDates));
        }
      } else {
        const maxDates = 7;
        if (dailyData.length <= maxDates) {
          setDates(dailyData);
        } else {
          setDates(dailyData.slice(dailyData.length - maxDates));
        }
      }
    } else {
      setDates([]);
    }
  }, [dailyData, isPhone, isTablet]);

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {totalOrDaily}
        </Typography>
        <Typography variant="h5" component="h2" gutterBottom>
          {region.label}
          {region.icon ? ` ${region.icon}` : ''}
        </Typography>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                {dates.map(({ date }) => {
                  const [
                    { value: month },
                    ,
                    { value: day },
                    ,
                    { value: year },
                  ] = dateTimeFormat.formatToParts(new Date(date));

                  return (
                    <TableCell align="right" key={date}>
                      {isPhone
                        ? `${day} ${month}`
                        : isTablet
                        ? `${day} ${month}`
                        : `${day} ${month} ${year}`}
                    </TableCell>
                  );
                })}
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell component="th" scope="row">
                  Infected
                </TableCell>
                {dates.map(({ date, confirmed }) => {
                  return (
                    <TableCell align="right" key={date}>
                      {confirmed.toLocaleString()}
                    </TableCell>
                  );
                })}
              </TableRow>
              {region.label !== GLOBAL && (
                <>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Active
                    </TableCell>
                    {dates.map(({ date, active }) => {
                      return (
                        <TableCell align="right" key={date}>
                          {active.toLocaleString()}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Recovered
                    </TableCell>
                    {dates.map(({ date, recovered }) => {
                      return (
                        <TableCell align="right" key={date}>
                          {recovered.toLocaleString()}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                </>
              )}
              <TableRow>
                <TableCell component="th" scope="row">
                  Deaths
                </TableCell>
                {dates.map(({ date, deaths }) => {
                  return (
                    <TableCell align="right" key={date}>
                      {deaths.toLocaleString()}
                    </TableCell>
                  );
                })}
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </CardContent>
    </Card>
  );
}

export default InfoCard;
