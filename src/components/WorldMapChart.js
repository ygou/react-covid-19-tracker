import React, { useRef, useEffect, useState } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { select, geoPath, geoMercator, max, scaleLinear } from 'd3';
import './WorldMapChart.css';
import useResizeObserver from '../accessories/useResizeObserver';
import data from '../config/world-geo.json';
import { Perspectives } from '../config/constants';

const useStyles = makeStyles({
  chart: {
    marginTop: '1rem',
    marginBottom: '1rem',
  },
});

/**
 * Component that renders a world map.
 */

function WorldMapChart({ regionsDaily, property }) {
  const svgRef = useRef();
  const wrapperRef = useRef();
  const dimensions = useResizeObserver(wrapperRef);
  const [selectedCountry, setSelectedCountry] = useState(null);
  const theme = useTheme();
  const classes = useStyles();
  const isPhone = useMediaQuery(theme.breakpoints.down('xs'));

  const svgStyle = {
    height: isPhone ? '300px' : '70vh',
  };

  // will be called initially and on every data change
  useEffect(() => {
    const svg = select(svgRef.current);

    const getMin = () => {
      return 0;
    };

    const getMax = () => {
      if (property === Perspectives.deathToll) {
        return max(regionsDaily, (summary) => summary.deaths);
      } else if (property === Perspectives.confirmed) {
        return max(regionsDaily, (summary) => summary.confirmed);
      } else if (property === Perspectives.active) {
        return max(regionsDaily, (summary) => summary.active);
      } else if (property === Perspectives.recovered) {
        return max(regionsDaily, (summary) => summary.recovered);
      } else if (property === Perspectives.confirmedOverPopulation) {
        return max(regionsDaily, (summary) => {
          let feature = data.features.find(
            (feature) => feature.properties['iso_a2'] === summary.iso2
          );
          if (feature && feature.properties['pop_est'] > 0) {
            return summary.confirmed / feature.properties['pop_est'];
          }
          return 0;
        });
      } else if (property === Perspectives.activeOverPopulation) {
        return max(regionsDaily, (summary) => {
          let feature = data.features.find(
            (feature) => feature.properties['iso_a2'] === summary.iso2
          );
          if (feature && feature.properties['pop_est'] > 0) {
            return summary.active / feature.properties['pop_est'];
          }
          return 0;
        });
      } else if (property === Perspectives.deathsOverPopulation) {
        return max(regionsDaily, (summary) => {
          let feature = data.features.find(
            (feature) => feature.properties['iso_a2'] === summary.iso2
          );
          if (feature && feature.properties['pop_est'] > 0) {
            return summary.deaths / feature.properties['pop_est'];
          }
          return 0;
        });
      } else if (property === Perspectives.deathsOverConfirmed) {
        return max(regionsDaily, (summary) => {
          return summary.iso2 &&
            summary.confirmed > 0 &&
            summary.confirmed >= summary.deaths
            ? summary.deaths / summary.confirmed
            : 0;
        });
      } else if (property === Perspectives.recoveredOverConfirmed) {
        return max(regionsDaily, (summary) => {
          return summary.iso2 &&
            summary.confirmed > 0 &&
            summary.confirmed >= summary.recovered
            ? summary.recovered / summary.confirmed
            : 0;
        });
      }

      return 0;
    };

    const minProp = getMin();
    const maxProp = getMax();

    const getMaxColor = () => {
      if (
        property === Perspectives.recovered ||
        property === Perspectives.recoveredOverConfirmed
      ) {
        return theme.palette.primary.green;
      } else {
        return theme.palette.primary.darkRed;
      }
    };

    const colorScale = scaleLinear()
      .domain([minProp, maxProp])
      .range([theme.palette.primary.grey, getMaxColor()]);

    const getValue = (feature) => {
      if (property === Perspectives.deathToll) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        return colorScale(region ? region.deaths : 0);
      } else if (property === Perspectives.confirmed) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        return colorScale(region ? region.confirmed : 0);
      } else if (property === Perspectives.active) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        return colorScale(region ? region.active : 0);
      } else if (property === Perspectives.recovered) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        return colorScale(region ? region.recovered : 0);
      } else if (property === Perspectives.confirmedOverPopulation) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        if (region && feature.properties['pop_est'] > 0) {
          return colorScale(region.confirmed / feature.properties['pop_est']);
        }
        return colorScale(0);
      } else if (property === Perspectives.activeOverPopulation) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        if (region && feature.properties['pop_est'] > 0) {
          return colorScale(region.active / feature.properties['pop_est']);
        }
        return colorScale(0);
      } else if (property === Perspectives.deathsOverPopulation) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        if (region && feature.properties['pop_est'] > 0) {
          return colorScale(region.deaths / feature.properties['pop_est']);
        }
        return colorScale(0);
      } else if (property === Perspectives.deathsOverConfirmed) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        if (region) {
          return colorScale(
            region.confirmed > 0 && region.confirmed >= region.deaths
              ? region.deaths / region.confirmed
              : 0
          );
        }
        return colorScale(0);
      } else if (property === Perspectives.recoveredOverConfirmed) {
        const region = regionsDaily.find(
          (summary) => summary.iso2 === feature.properties.iso_a2
        );
        if (region) {
          return colorScale(
            region.confirmed > 0 && region.confirmed >= region.recovered
              ? region.recovered / region.confirmed
              : 0
          );
        }
        return colorScale(0);
      }
      return getMin();
    };

    const getDescription = (feature) => {
      if (!feature) {
        return '';
      }

      let description = `${feature.properties.name}`;

      const region = regionsDaily.find(
        (summary) => summary.iso2 === feature.properties.iso_a2
      );

      if (region) {
        if (property === Perspectives.deathToll) {
          description += ` deaths: ${region.deaths.toLocaleString()}`;
        } else if (property === Perspectives.confirmed) {
          description += ` infected: ${region.confirmed.toLocaleString()}`;
        } else if (property === Perspectives.active) {
          description += ` active: ${region.active.toLocaleString()}`;
        } else if (property === Perspectives.recovered) {
          description += ` recovered: ${region.recovered.toLocaleString()}`;
        } else if (
          property === Perspectives.confirmedOverPopulation &&
          feature.properties['pop_est'] > 0
        ) {
          description += ` infected over population: ${(
            (region.confirmed * 1000) /
            feature.properties['pop_est']
          ).toFixed(3)}‰`;
        } else if (
          property === Perspectives.activeOverPopulation &&
          feature.properties['pop_est'] > 0
        ) {
          description += ` active over population: ${(
            (region.active * 1000) /
            feature.properties['pop_est']
          ).toFixed(3)}‰`;
        } else if (
          property === Perspectives.deathsOverPopulation &&
          feature.properties['pop_est'] > 0
        ) {
          description += ` deaths over population: ${(
            (region.deaths * 1000) /
            feature.properties['pop_est']
          ).toFixed(3)}‰`;
        } else if (property === Perspectives.deathsOverConfirmed) {
          description += ` deaths over infected: ${(region.confirmed > 0 &&
          region.confirmed >= region.deaths
            ? (region.deaths * 100) / region.confirmed
            : 0
          ).toFixed(2)}%`;
        } else if (property === Perspectives.recoveredOverConfirmed) {
          description += ` recovered over infected: ${(region.confirmed > 0 &&
          region.confirmed >= region.recovered
            ? (region.recovered * 100) / region.confirmed
            : 0
          ).toFixed(2)}%`;
        }
      }

      return description;
    };

    // use resized dimensions
    // but fall back to getBoundingClientRect, if no dimensions yet.
    const { width, height } =
      dimensions || wrapperRef.current.getBoundingClientRect();
    // const { width, height } = svgRef.current.getBoundingClientRect();

    // projects geo-coordinates on a 2D plane
    const projection = geoMercator()
      .fitSize([width, height], selectedCountry || data)
      .precision(100);

    // takes geojson data,
    // transforms that into the d attribute of a path element
    const pathGenerator = geoPath().projection(projection);

    // render each country
    svg
      .selectAll('.country')
      .data(data.features)
      .join('path')
      .on('click', (feature) => {
        setSelectedCountry(selectedCountry === feature ? null : feature);
      })
      .attr('class', 'country')
      .transition()
      .attr('fill', getValue)
      .attr('d', (feature) => pathGenerator(feature));

    // render text
    svg
      .selectAll('.label')
      .data([selectedCountry])
      .join('text')
      .attr('class', 'label')
      .text(getDescription)
      .attr('x', 10)
      .attr('y', 25);
  }, [
    dimensions,
    property,
    regionsDaily,
    selectedCountry,
    theme.palette.primary,
  ]);

  return (
    <div ref={wrapperRef} className={classes.chart}>
      <svg ref={svgRef} style={svgStyle}></svg>
    </div>
  );
}

export default WorldMapChart;
