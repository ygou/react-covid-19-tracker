import React, { useState, useEffect, useRef } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Line } from 'react-chartjs-2';
import 'chartjs-plugin-zoom';

const useStyles = makeStyles({
  chart: {
    marginTop: '1rem',
    height: '60vh',
  },
});

const savedShowInfected = localStorage.getItem('showInfected');
const savedShowDeaths = localStorage.getItem('showDeaths');

export default function TimelineChart({ dailyData, region, type }) {
  const [showInfected, setShowInfected] = useState(
    !savedShowInfected ? false : savedShowInfected === 'true' ? true : false
  );
  const [showDeaths, setShowDeaths] = useState(
    !savedShowDeaths ? true : savedShowDeaths === 'true' ? true : false
  );
  const theme = useTheme();
  const classes = useStyles();

  const chartReference = useRef();

  useEffect(() => {
    if (
      chartReference &&
      chartReference.current &&
      chartReference.current.chartInstance
    ) {
      const chartInstance = chartReference.current.chartInstance;
      chartInstance.resetZoom();
    }
  }, [region]);

  const isPhone = useMediaQuery(theme.breakpoints.down('xs'));
  const isTablet = useMediaQuery(theme.breakpoints.down('sm'));

  const lineChart = dailyData[0] ? (
    <Line
      data={{
        labels: dailyData.map(({ date }) => date),
        datasets: [
          {
            data: dailyData.map((data) => data.confirmed),
            label: 'Infected',
            borderColor: theme.palette.primary.main,
            fill: true,
            hidden: !showInfected,
          },
          {
            data: dailyData.map((data) => data.deaths),
            label: 'Deaths',
            borderColor: theme.palette.primary.darkRed,
            backgroundColor: theme.palette.primary.lightRed,
            fill: true,
            hidden: !showDeaths,
          },
        ],
      }}
      options={{
        title: { display: false, text: region },
        maintainAspectRatio: false,
        legend: {
          onClick: function (e, legendItem) {
            if (legendItem.text === 'Infected') {
              localStorage.setItem('showInfected', legendItem.hidden);
              setShowInfected(legendItem.hidden);
            } else {
              localStorage.setItem('showDeaths', legendItem.hidden);
              setShowDeaths(legendItem.hidden);
            }
          },
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                callback: (label) => {
                  return label.toLocaleString();
                },
                precision: 0,
                autoSkip: true,
                maxTicksLimit: isPhone ? 5 : isTablet ? 7 : 10,
              },
              type: type,
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              }: ${tooltipItem.yLabel.toLocaleString()}`;
            },
          },
        },
        zoom: {
          enabled: true,
          mode: 'x',
        },
        pan: {
          enabled: true,
          mode: 'x',
        },
      }}
      ref={chartReference}
    />
  ) : null;

  return <div className={classes.chart}>{lineChart}</div>;
}
