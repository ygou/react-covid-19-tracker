import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  title: {
    marginTop: '4rem',
    marginBottom: '2rem',
  },
});

export default function SectionTitle({ id, title }) {
  const classes = useStyles();
  return (
    <div className={classes.title} id={id}>
      <Typography
        variant="h5"
        data-testid="section-title"
        gutterBottom={true}
        color="inherit"
        align="center"
        variantMapping={{ h5: 'h2' }}
      >
        {title}
      </Typography>
    </div>
  );
}
