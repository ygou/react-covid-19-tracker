import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import AppHeader from '../AppHeader';

describe('AppHeader', () => {
  // Smoke test:
  // This test mounts a component and makes sure that it does not throw during rendering.
  // Tests like this provide a lot of value with very little effort so they are great as a starting point.
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
      <Router>
        <AppHeader title="Hello World!" />
      </Router>,
      div
    );
  });

  it('renders the app name', () => {
    const appName = 'Covid-19 Tracker';
    const { getByTestId } = render(
      <Router>
        <AppHeader title={appName} />
      </Router>
    );
    expect(getByTestId('app-name')).toHaveTextContent(appName);
  });
});
