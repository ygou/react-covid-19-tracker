import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import TimelineIcon from '@material-ui/icons/Timeline';
import GpsFixedIcon from '@material-ui/icons/GpsFixed';
import InfoIcon from '@material-ui/icons/Info';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import PropTypes from 'prop-types';
import TrackerLogo from '../assets/covid19_tracker_logo.png';
import { HashLink } from 'react-router-hash-link';

// https://github.com/mui-org/material-ui/issues/15903
const ForwardHashLink = React.forwardRef((props, ref) => (
  <HashLink {...props} innerRef={ref} />
));

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    borderTop: `5px solid ${theme.palette.primary.lightRed}`,
    display: 'flex',
  },
  offset: theme.mixins.toolbar,
  logo: {
    height: '50px',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  appLink: {
    display: 'flex',
    alignItems: 'center',
    textDecoration: 'none',
  },
  appLinkText: {
    color: theme.palette.primary.white,
  },
}));

export default function AppHeader({ id, title }) {
  const [open, setOpen] = React.useState(false);
  const [justOpen, setJustOpen] = React.useState(false);
  const classes = useStyles();
  const theme = useTheme();

  const handleDrawerOpen = () => {
    setOpen(true);
    setJustOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleClickAway = (e) => {
    if (open && justOpen) {
      setJustOpen(false);
    } else if (open && !justOpen) {
      setOpen(false);
    }
  };

  return (
    <Container maxWidth="lg" id={id}>
      <AppBar position="fixed" className={classes.root}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={handleDrawerOpen}
          >
            <MenuIcon />
          </IconButton>
          <ForwardHashLink to="/" className={classes.appLink}>
            <img
              src={TrackerLogo}
              className={classes.logo}
              alt="COVID-19 Tracker Logo"
            />
            <Typography
              variant="h6"
              data-testid="app-name"
              className={classes.appLinkText}
            >
              {title}
            </Typography>
          </ForwardHashLink>
        </Toolbar>
      </AppBar>
      <div className={classes.offset} />
      <ClickAwayListener onClickAway={handleClickAway}>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'ltr' ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </IconButton>
          </div>
          <Divider />
          <List>
            {[
              {
                text: 'Timeline',
                Icon: TimelineIcon,
                href: '/#timeline',
              },
              {
                text: 'Spread',
                Icon: GpsFixedIcon,
                href: '/#spread',
              },
              {
                text: 'About',
                Icon: InfoIcon,
                href: '/about',
              },
            ].map(({ Icon, href, text }) => (
              <ListItem
                button
                key={text}
                component={ForwardHashLink}
                to={href}
                onClick={handleDrawerClose}
              >
                <ListItemIcon>
                  <Icon />
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </Drawer>
      </ClickAwayListener>
    </Container>
  );
}

AppHeader.propTypes = {
  title: PropTypes.string.isRequired,
};
