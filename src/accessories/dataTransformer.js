const getDaily = (current, previous) => {
  return current - previous >= 0 ? current - previous : 0;
};

export const totalToDaily = (data) => {
  return data.map(({ date, deaths, confirmed, recovered, active }, index) => {
    if (index === 0) {
      return { date, deaths, confirmed, recovered, active };
    } else {
      return {
        date,
        deaths: getDaily(deaths, data[index - 1].deaths),
        confirmed: getDaily(confirmed, data[index - 1].confirmed),
        recovered: getDaily(recovered, data[index - 1].recovered),
        active: getDaily(active, data[index - 1].active),
      };
    }
  });
};
