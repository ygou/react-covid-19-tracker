import React, { lazy, Suspense } from 'react';
import ReactDom from 'react-dom';
import 'typeface-roboto';
import ReactGA from 'react-ga';
import * as serviceWorker from './serviceWorker';

ReactGA.initialize('UA-167884612-1');
ReactGA.pageview(window.location.pathname + window.location.search);

const App = lazy(() => import('./App'));

ReactDom.render(
  <Suspense fallback={<></>}>
    <App />
  </Suspense>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
