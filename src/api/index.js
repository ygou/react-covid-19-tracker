import axios from 'axios';
import { GLOBAL, URL } from '../config/constants';
import { isGlobalUpdated, isRegionUpdated, addDaily, getDaily } from './cache';

const fetchWorldDaily = async () => {
  const { data } = await axios.get(`${URL.WORLD_DAILY}`);
  if (data) {
    const worldDaily = data.map(
      ({ confirmed, deaths, recovered, reportDate: date }) => ({
        confirmed: confirmed.total,
        deaths: deaths.total,
        recovered: recovered.total,
        active: confirmed.total - deaths.total - recovered.total,
        date,
      })
    );
    addDaily({ region: GLOBAL, daily: worldDaily });
    return worldDaily;
  } else {
    console.error(`Daily stats not available for the world`);
    return [];
  }
};

const fetchRegionDaily = async (region) => {
  const { data } = await axios.get(`${URL.REGION_DAILY}/${region}`);
  if (data && data.dailyStats) {
    const regionDaily = data.dailyStats.map(
      ({ confirmed, deaths, recovered, reportDate: date }) => ({
        confirmed,
        deaths,
        recovered,
        active: confirmed - deaths - recovered,
        date,
      })
    );
    addDaily({ region, daily: regionDaily });
    return regionDaily;
  } else {
    console.error(`Daily stats not available for ${region}`);
    return [];
  }
};

export const fetchDailyData = async (region) => {
  try {
    if (GLOBAL === region) {
      if (isGlobalUpdated()) {
        return getDaily(GLOBAL);
      } else {
        return fetchWorldDaily();
      }
    } else {
      if (!isGlobalUpdated()) {
        fetchWorldDaily();
      }
      if (isRegionUpdated(region)) {
        return getDaily(region);
      } else {
        return fetchRegionDaily(region);
      }
    }
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const fetchRegions = async () => {
  try {
    const { data } = await axios.get(`${URL.REGIONS}`);

    return data.map(({ name, iso2, icon }) => {
      return { label: name, iso2, icon };
    });
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const fetchRegionsDaily = async (timeframe) => {
  try {
    const { data } = await axios.get(`${URL[timeframe]}`);

    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
};
