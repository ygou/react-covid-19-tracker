import { GLOBAL, CACHE_TTL } from '../../config/constants';

const state = {
  daily: {
    [GLOBAL]: {
      value: [], // Sorted by date from oldest to newest
      lastUpdate: null, // The numeric value of the specified date as the number of milliseconds since January 1, 1970, 00:00:00 UTC
    },
  },
};

export const isGlobalUpdated = () => {
  if (state.daily[GLOBAL].value.length <= 0) {
    return false;
  }

  if (!state.daily[GLOBAL].lastUpdate) {
    return false;
  }

  const current = new Date().getTime();
  return current < state.daily[GLOBAL].lastUpdate + CACHE_TTL * 1000;
};

export const isRegionUpdated = (region) => {
  if (
    region in state.daily &&
    state.daily[region].value &&
    state.daily[region].value.length > 0
  ) {
    const globalDays = state.daily[GLOBAL].value.length;
    if (globalDays <= 0) {
      return false;
    }

    const latestGlobalDate = state.daily[GLOBAL].value[globalDays - 1];
    const latestRegionDate =
      state.daily[region].value[state.daily[region].value.length - 1];
    return latestGlobalDate.date === latestRegionDate.date;
  } else {
    return false;
  }
};

export const addDaily = ({ region, daily }) => {
  if (region === GLOBAL) {
    state.daily[GLOBAL].lastUpdate = new Date().getTime();
    state.daily[GLOBAL].value = daily;
  } else {
    state.daily[region] = { value: daily };
  }
};

export const getDaily = (region) => {
  if (region in state.daily && state.daily[region]) {
    return state.daily[region].value;
  }
  return [];
};
